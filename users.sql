-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2016 at 07:36 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sumonmhd_php27`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `verification` varchar(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  `is_admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `unique_id`, `verification`, `username`, `password`, `email`, `is_active`, `is_admin`) VALUES
(3, '57a22e591e1b1', '57', 'Avra01', '12345', 'avra.saha@yahoo.com', 1, 0),
(4, '57a22ed85d4c9', '57a22ed85d4', 'Avra02', '123456', 'wsoshi636@yahoo.com', 1, 0),
(5, '57a42aed96ed9', '57a42aed96e', 'Avra03', '12345', 'md.nepolian@gmail.com', 0, 0),
(6, '57a42b49d5bd6', '57a42b49d5b', 'Avra04', '123456', 'mmd.nepolian@gmail.com005067', 1, 0),
(7, '57a431b9b70a3', '57a431b9b70', 'saha01', '12345', 'tuntune@gmail.com', 1, 0),
(8, '57a431f365d05', '57a431f365d', 'saha02', '12345', 'saha01@gmail.com', 1, 0),
(9, '57a4aa3ea13c0', '57a4aa3ea13', 'saha03', '12345', 'saha03@gmail.com', 1, 0),
(10, '57a4aa64e65f6', '57a4aa64e4a', 'saha04', '123456', 'saha04@gmail.com', 1, 0),
(11, '57a5668ce568a', '57a5668ce56', '', '', '', 0, 0),
(12, '57a56dedbf54d', '57a56dedbf5', 'saha05', '12345', 'saha05@gmail.com', 0, 0),
(13, '57a56e363ef2f', '57a56e363ef', 'saha06', '12345', 'saha06@gmail.com', 0, 0),
(14, '57a574a202a68', '57a574a202a', 'saha07', '12345', 'saha07@gmail.com', 1, 0),
(15, '57a584fb6a430', '57a584fb6a4', 'saha08', '12345', 'saha08@gmail.com', 1, 0),
(16, '57a59bc4cadb4', '57a59bc4cad', 'Metho01', '12345', 'metho01@podder.com', 1, 0),
(17, '57a59c95a53ed', '57a59c95a53', 'saha09', '12345', 'saha09@gmail.com', 0, 0),
(18, '57a59da0d1fdf', '57a59da0d1f', 'saha10', '12345', 'saha10@gmail.com', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

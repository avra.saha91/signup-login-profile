<?php

namespace App\users;

use PDO;

class users {

    public $dbuser = 'root';
    public $dbpass = '';
    public $username = '';
    public $password = '';
    public $email = '';
    public $conn = '';
    public $data = '';
    public $userexist = '';
    public $verification_id = '';
    public $last_id = '';
    
    public $full_name = '';
    public $father_name = '';
    public $mother_name = '';
    public $gender = '';
    public $dateofbirth = '';
    public $mobile_number, $occupation, $education_status, $religious, $marital_status, $current_job, $nationality, $interested, $bio_data, $n_id, $passport_number, $skill_area, $language, $blood_group, $fax_number, $hight, $address, $web_address, $profile_pic, $other , $filename= '';
    //public $address = serialize($_post($arr));
    public $addr1, $addr2, $addr3 ,$addr4 ,$addr5 ='';
    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:host=localhost;dbname=sumonmhd_php27', $this->dbuser, $this->dbpass);
        
        
        $conn = mysql_connect('localhost', 'root', '') or die('opps!unableto to connect from database');
        mysql_select_db('sumonmhd_php27')or die('unable to connect to database');
    }

    public function prepare($data = '') {
//        echo '<pre>';
//        print_r ($data);
//        die();
        
        if(isset($_REQUEST['submit']))
{
  $this->filename=  $_FILES["imgfile"]["name"];
  if ((($_FILES["imgfile"]["type"] == "image/gif")|| ($_FILES["imgfile"]["type"] == "image/jpeg") || ($_FILES["imgfile"]["type"] == "image/png")  || ($_FILES["imgfile"]["type"] == "image/pjpeg")) && ($_FILES["imgfile"]["size"] < 200000))
  {
    if(file_exists($_FILES["imgfile"]["name"]))
    {
      echo "File name exists.";
    }
    else
    {
      move_uploaded_file($_FILES["imgfile"]["tmp_name"],"uploads/$filename");
      echo "Upload Successful . <a href='uploads/$filename'>Click here</a> to view the uploaded image";
    }
  }
  else
  {
    echo "invalid file.";
  }
}
        
        
        if (!empty($data['uname'])) {
            $this->username = $data['uname'];
        }

        if (!empty($data['pw1'])) {
            $this->password = $data['pw1'];
        }

        if (!empty($data['email'])) {
            $this->email = $data['email'];
        }
        if (!empty($data['vid'])) {
            $this->verification_id = $data['vid'];
        }


        if (!empty($data['firstname'])) {
            $this->full_name = $data['firstname'];
        }
        if (!empty($data['Mobile'])) {
            $this->mobile_number = $data['Mobile'];
        }
        if (!empty($data['Fnum'])) {
            $this->fax_number = $data['Fnum'];
        }
        if (!empty($data['edu'])) {
            $this->education_status = $data['edu'];
        }
        if (!empty($data['occupation'])) {
            $this->occupation = $data['occupation'];
        }
        if (!empty($data['bdata'])) {
            $this->bio_data = $data['bdata'];
        }
        if (!empty($data['fname'])) {
            $this->father_name = $data['fname'];
        }
        if (!empty($data['nId'])) {
            $this->n_id = $data['nId'];
        }
        if (!empty($data['gender'])) {
            $this->gender = $data['gender'];
        }
        if (!empty($data['job'])) {
            $this->current_job = $data['job'];
        }
        if (!empty($data['nat'])) {
            $this->nationality = $data['nat'];
        }
        if (!empty($data['skill'])) {
            $this->skill_area = $data['skill'];
        }
        if (!empty($data['mname'])) {
            $this->mother_name = $data['mname'];
        }
        if (!empty($data['pnum'])) {
            $this->passport_number = $data['pnum'];
        }
        if (!empty($data['dbirth'])) {
            $this->dateofbirth = $data['dbirth'];
        }
        if (!empty($data['mstatus'])) {
            $this->marital_status = $data['mstatus'];
        }
        if (!empty($data['inter'])) {
            $this->interested = $data['inter'];
        }
        if (!empty($data['lan'])) {
            $this->language = $data['lan'];
        }
       
        if (!empty($data['wa'])) {
            $this->web_address = $data['wa'];
        }
        if (!empty($data['bgroup'])) {
            $this->blood_group = $data['bgroup'];
        }
        if (!empty($data['addr1'])) {
            $this->addr1 = $data['addr1'];
        }
        if (!empty($data['addr2'])) {
            $this->addr2 = $data['addr2'];
        }
        if (!empty($data['addr3'])) {
            $this->addr3 = $data['addr3'];
        }
        if (!empty($data['addr4'])) {
            $this->addr4 = $data['addr4'];
        }
        if (!empty($data['addr5'])) {
            $this->addr5 = $data['addr5'];
        }
        $arr=array("$this->addr1","$this->addr2","$this->addr3","$this->addr4","$this->addr5");
//        print_r($arr);
//        die();
        $this->address = serialize($arr);

        return $this;
    }

    public function index() {
        try {
            $query = "SELECT * FROM `users` ";
            $q = $this->conn->query($query) or die('Unable to query');
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this->data;
        return $this;
    }

    public function show() {
        
    }

    public function signup() {
        try {
            $usrname = "'$this->username'";
            $qr = "SELECT * FROM users WHERE username=" . $usrname;
            $STH = $this->conn->prepare($qr);
            $STH->execute();
            $user = $STH->fetch(PDO::FETCH_ASSOC);

            $email = "'$this->email'";
            $qr = "SELECT * FROM users WHERE email=" . $email;
            $STH = $this->conn->prepare($qr);
            $STH->execute();
            $user2 = $STH->fetch(PDO::FETCH_ASSOC);
            if (!empty($user)) {
                $_SESSION['Message'] = "Username already exists";
                header('location:signup.php');
            } elseif (!empty($user2)) {
                $_SESSION['Message'] = "Already register with this email";
                header('location:signup.php');
            } else {
                $verification_code = uniqid();
                $query = "INSERT INTO users(id, unique_id, verification, username, password, email, is_active, is_admin)
    VALUES(:id, :uid, :vrfsn, :un, :pw, :email, :ia, :iad)";
                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':id' => null,
                    ':uid' => uniqid(),
                    ':vrfsn' => $verification_code,
                    ':un' => $this->username,
                    ':pw' => $this->password,
                    ':email' => $this->email,
                    ':ia' => 0,
                    ':iad' => 0,
                ));
                $_SESSION['Message'] = "Successfully signup. Login for continue";
//                $msg = "Click the below link for verify your email address.<br/> http://test.sumonmhd.com/Views/verify.php?vid=$verification_code";
////                echo $msg;
////                die();
//                $msg = wordwrap($msg, 70);
                // mail("$this->email", "sumonmhd.com", $msg);
                $last_id = $this->conn->lastInsertId();
//                echo $last_id;
//                die();
//                $query = "INSERT INTO `profile` (`id`, `users_id`, `full_name`, `father_name`, `mother_name`, `gender`, `date-of-birth`, `mobile_number`, `occupation`, `education_status`, `religious`, `marital_status`, `current_job`, `nationality`, `interested`, `bio_data`, `n_id`, `passport_number`, `skill_area`, `language`, `blood_group`, `fax_number`, `hight`, `address`, `web_address`, `profile_pic`, `other`) "
//                        . "VALUES (NULL,$last_id, '$this->full_name', '$this->father_name', '$this->mother_name', '$this->gender', '$this->dateofbirth', '$this->mobile_number', '$this->occupation', '$this->education_status', '$this->religious', '$this->marital_status', '$this->current_job', '$this->nationality', '$this->interested', '$this->bio_data', '$this->n_id', '$this->passport_number', '$this->skill_area', '$this->language', '$this->blood_group', '$this->fax_number', '$this->hight', '$this->address', '$this->web_address', '$this->profile_pic', '$this->other')";
////        echo "<pre>";
////        print_r($query);
////        die();
//                if (mysql_query($query)) {
//                    $_SESSION['Passage'] = "Data successfully created";
//                }
//                header('location:profile.php?Message="Successfullt Submitted"');




               header('location:login.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function login() {
        $usrname = "'$this->username'";
        $password = "'$this->password'";
        $qr = "select * from users where username =  $usrname && password = $password";
        $STH = $this->conn->prepare($qr);
        $STH->execute();
        $user = $STH->fetch(PDO::FETCH_ASSOC);
//        echo "<pre>";
//        print_r($user);
//        die();
        if (isset($user) && !empty($user)) {
            if ($user['is_active'] == 0) {
                $_SESSION['Message'] = "<h3>Your account not verified yet. Check your email and verify</h3>";
                header('location:login.php');
            } else {
                //$_SESSION['user'] = $user;
                header('location:profile.php');
            }
        } else {
            $_SESSION['Message'] = "<h3>invalid username or password</h3>";
            header('location:login.php');
        }
    }

    public function verification() {
        $verification_code = "'" . $this->verification_id . "'";
        $qr = "SELECT * FROM users WHERE verification = $verification_code";
        $STH = $this->conn->prepare($qr);
        $STH->execute();
        $user = $STH->fetch(PDO::FETCH_ASSOC);
        echo "<pre>";
        print_r($user);
        die();

        if (isset($user['verification'])) {
            $verification_code = "'" . $this->verification_id . "'";
            $query = "UPDATE users SET is_active = 1 WHERE verification =" . $verification_code;
            $STH = $this->conn->prepare($query);
            if ($STH->execute()) {
                $_SESSION['Message'] = "<h1>You're verified now. Thank you !</h1>";
                header('location:login.php');
            }
        }
    }

    public function profile() {

        $query = "INSERT INTO `profile` (`id`, `users_id`, `full_name`, `father_name`, `mother_name`, `gender`, `date-of-birth`, `mobile_number`, `occupation`, `education_status`, `religious`, `marital_status`, `current_job`, `nationality`, `interested`, `bio_data`, `n_id`, `passport_number`, `skill_area`, `language`, `blood_group`, `fax_number`, `hight`, `address`, `web_address`, `profile_pic`, `other`) "
                . "VALUES (NULL, ' $this->last_id', '$this->full_name', '$this->father_name', '$this->mother_name', '$this->gender', '$this->dateofbirth', '$this->mobile_number', '$this->occupation', '$this->education_status', '$this->religious', '$this->marital_status', '$this->current_job', '$this->nationality', '$this->interested', '$this->bio_data', '$this->n_id', '$this->passport_number', '$this->skill_area', '$this->language', '$this->blood_group', '$this->fax_number', '$this->hight', '$this->address', '$this->web_address', '$this->filename', '$this->other')";
//        echo "<pre>";
//        print_r($query);
//        die();
        if (mysql_query($query)) {
            $_SESSION['Passage'] = "Data successfully created";
        }
        header('location:profile.php?Message="Successfullt Submitted"');
//        try {
//            $query = "INSERT INTO profile(id, users_id, unique_id, full_name, father_name, mother_name, gender, date-of-birth, mobile_number, occupation, education_status, religious, marital_status, current_job, nationality, interested, bio_data, n_id, passport_number, skill_area, language, blood_group, fax_number, hight, address, web_address, profile_pic, other)
//    VALUES(:id,:usid, :unid, :fln, :fn, :mn,:gn, :db, :mn, :occ,:eds,:rel,:mari,:cuj,:nat,:int,:biod,:nid,:passn,:ska,:lan,:blg,:fxn,:hig,:add,:wea,:pp,:ot)";
//            $stmt = $this->conn->prepare($query);
//            $stmt->execute(array(
//                ':id' => null,
//                ':usid' => $this->lastid,
//                ':unid' => uniqid(),
//                ':fln' => $this->full_name,
//                ':fn' => $this->father_name,
//                ':mn' => $this->mother_name,
//                ':gn' => $this->gender,
//                ':db' => $this->dateofbirth,
//                ':mn' => $this->mobile_number,
//                ':occ' => $this->occupation,
//                ':eds' => $this->education_status,
//                ':rel' => $this->religious,
//                ':mari' => $this->marital_status,
//                ':cuj' => $this->current_job,
//                ':nat' => $this->nationality,
//                ':int' => $this->interested,
//                ':biod' => $this->bio_data,
//                ':nid' => $this->n_id,
//                ':passn' => $this->passport_number,
//                ':ska' => $this->skill_area,
//                ':lan' => $this->language,
//                ':blg' => $this->blood_group,
//                ':fxn' => $this->fax_number,
//                ':hig' => $this->hight,
//                ':add' => $this->address,
//                ':wea' => $this->web_address,
//                ':pp' => $this->profile_pic,
//                ':ot' => $this->other,
//            ));
//        } catch (PDOException $e) {
//            echo 'Error: ' . $e->getMessage();
//        }header('location:profile.php');
    }

}

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2016 at 07:36 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sumonmhd_php27`
--

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `mother_name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `date-of-birth` int(11) NOT NULL,
  `mobile_number` int(11) NOT NULL,
  `occupation` varchar(255) NOT NULL,
  `education_status` varchar(255) NOT NULL,
  `religious` varchar(255) NOT NULL,
  `marital_status` varchar(255) NOT NULL,
  `current_job` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `interested` varchar(255) NOT NULL,
  `bio_data` varchar(255) NOT NULL,
  `n_id` int(25) NOT NULL,
  `passport_number` varchar(255) NOT NULL,
  `skill_area` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `blood_group` varchar(255) NOT NULL,
  `fax_number` int(25) NOT NULL,
  `hight` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `web_address` varchar(255) NOT NULL,
  `profile_pic` datetime NOT NULL,
  `other` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `users_id`, `full_name`, `father_name`, `mother_name`, `gender`, `date-of-birth`, `mobile_number`, `occupation`, `education_status`, `religious`, `marital_status`, `current_job`, `nationality`, `interested`, `bio_data`, `n_id`, `passport_number`, `skill_area`, `language`, `blood_group`, `fax_number`, `hight`, `address`, `web_address`, `profile_pic`, `other`) VALUES
(1, 0, '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', '', '', 0, 0, '', '', '0000-00-00 00:00:00', ''),
(2, 0, 'Avra Saha', 'swapan saha', 'shibani saha', 'Male', 0, 1941, 'student', 'B.sc', 'Hindu', 'Unmarride', 'baker', 'Bangladesh', 'Bike', 'fgegegvnbc', 211646516, 'gdd16546dg', 'php', 'bangla', 'A+', 34134, 6, '', 'www.avra.info', '0000-00-00 00:00:00', 'sfsdf'),
(3, 0, 'Avra Saha', 'swapan saha', 'shibani saha', 'Male', 0, 1941, 'student', 'B.sc', 'Hindu', 'Unmarride', 'baker', 'Bangladesh', 'Bike', 'fgegegvnbc', 211646516, 'gdd16546dg', 'php', 'bangla', 'A+', 34134, 6, '', 'www.avra.info', '0000-00-00 00:00:00', 'sfsdf'),
(4, 0, 'Avra Saha', 'swapan saha', 'shibani saha', 'Male', 0, 1941, 'student', 'B.sc', 'Hindu', 'Unmarride', 'baker', 'Bangladesh', 'Bike', 'fgegegvnbc', 211646516, 'gdd16546dg', 'php', 'bangla', 'A+', 34134, 6, '', 'www.avra.info', '0000-00-00 00:00:00', 'sfsdf'),
(6, 0, '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', 'Bangla', '', 0, 0, '', '', '0000-00-00 00:00:00', ''),
(7, 0, 'Avra Saha', 'Swapan Saha', 'shibani saha', 'male', 0, 1918299861, 'student', 'B.sc', '', 'unmarried', 'baker', '', 'Bike', '', 2147483647, 'ada2154', 'PHP', 'Hindi', '', 2142545, 0, '', 'www.avra.info', '0000-00-00 00:00:00', ''),
(9, 0, 'Avra Saha', 'Swapan Saha', 'shibani saha', '', 0, 1918299861, '', '', '', '', '', '', '', '', 0, '', '', 'Bangla', '', 2142545, 0, '', '', '0000-00-00 00:00:00', ''),
(10, 0, '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', 'Bangla', '', 0, 0, '', '', '0000-00-00 00:00:00', ''),
(11, 0, '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', 'Bangla', '', 0, 0, '', '', '0000-00-00 00:00:00', ''),
(12, 0, 'Avra Saha', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', 'Bangla', '', 0, 0, '', '', '0000-00-00 00:00:00', ''),
(13, 0, 'Avra Saha', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', 'Bangla', '', 0, 0, '', '', '0000-00-00 00:00:00', ''),
(14, 0, 'metho', 'Goutom', 'Merra rani', '', 0, 1918299861, '', '', '', '', '', '', '', '', 2147483647, '', '', 'Bangla', '', 0, 0, '', '', '0000-00-00 00:00:00', ''),
(15, 0, 'Avra Saha', 'Swapan Saha', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', 'Bangla', '', 0, 0, 'a:5:{i:0;s:5:"addr1";i:1;s:5:"addr2";i:2;s:5:"addr3";i:3;s:5:"addr4";i:4;s:5:"addr5";}', '', '0000-00-00 00:00:00', ''),
(16, 0, 'Avra Saha', 'Swapan Saha', 'shibani saha', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', 'Bangla', '', 0, 0, 'a:5:{i:0;s:5:"addr1";i:1;s:5:"addr2";i:2;s:5:"addr3";i:3;s:5:"addr4";i:4;s:5:"addr5";}', '', '0000-00-00 00:00:00', ''),
(17, 0, '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', 'Bangla', '', 0, 0, 'a:5:{i:0;s:5:"addr1";i:1;s:5:"addr2";i:2;s:5:"addr3";i:3;s:5:"addr4";i:4;s:5:"addr5";}', '', '0000-00-00 00:00:00', ''),
(18, 0, 'Avra Saha', 'Swapan Saha', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', 'Bangla', '', 0, 0, 'a:5:{i:0;s:5:"addr1";i:1;s:5:"addr2";i:2;s:5:"addr3";i:3;s:5:"addr4";i:4;s:5:"addr5";}', '', '0000-00-00 00:00:00', ''),
(19, 0, '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', 'Bangla', '', 0, 0, 'a:5:{i:0;s:5:"badda";i:1;s:5:"dhaka";i:2;s:4:"1212";i:3;s:5:"dhaka";i:4;s:5:"dhaka";}', '', '0000-00-00 00:00:00', ''),
(20, 0, 'suvra', 'Swapan Saha', 'shibani saha', '', 0, 0, '', '', '', '', '', '', '', '', 0, '', '', 'Bangla', '', 0, 0, 'a:5:{i:0;s:5:"badda";i:1;s:5:"dhaka";i:2;s:4:"1212";i:3;s:5:"dhaka";i:4;s:5:"dhaka";}', '', '0000-00-00 00:00:00', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
